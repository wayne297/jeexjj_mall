<#--
/****************************************************
 * Description: 商品表的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/item/save" id=tabId>
   <input type="hidden" name="id" value="${item.id}"/>
   
   <@formgroup title='商品标题'>
	<input type="text" name="title" value="${item.title}" >
   </@formgroup>
   <@formgroup title='商品卖点'>
	<input type="text" name="sellPoint" value="${item.sellPoint}" >
   </@formgroup>
   <@formgroup title='商品价格'>
	<input type="text" name="price" value="${item.price}" >
   </@formgroup>
   <@formgroup title='库存数量'>
	<input type="text" name="num" value="${item.num}" check-type="number">
   </@formgroup>
   <@formgroup title='售卖数量限制'>
	<input type="text" name="limitNum" value="${item.limitNum}" check-type="number">
   </@formgroup>
   <@formgroup title='商品图片'>
	<input type="text" name="image" value="${item.image}" >
   </@formgroup>
   <@formgroup title='所属分类'>
	<input type="text" name="cid" value="${item.cid}" check-type="number">
   </@formgroup>
   <@formgroup title='商品状态 1正常 0下架'>
	<input type="text" name="status" value="${item.status}" check-type="number">
   </@formgroup>
   <@formgroup title='创建时间'>
	<@datetime name="created" dateValue=item.created  default=true/>
   </@formgroup>
   <@formgroup title='更新时间'>
	<@datetime name="updated" dateValue=item.updated  default=true/>
   </@formgroup>
</@input>
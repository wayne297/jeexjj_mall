<#--
/****************************************************
 * Description: t_mall_panel_content的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl"> 
<@list id=tabId>
	<thead>
		<tr>
			<th><input type="checkbox" class="bscheckall"></th>
	        <th>所属板块id</th>
	        <th>类型 0关联商品 1其他链接</th>
	        <th>关联商品id</th>
	        <th>sort_order</th>
	        <th>其他链接</th>
	        <th>pic_url</th>
	        <th>3d轮播图备用</th>
	        <th>3d轮播图备用</th>
	        <th>created</th>
	        <th>updated</th>
	        <th>操作</th>
		</tr>
	</thead>
	<tbody>
		<#list page.items?if_exists as item>
		<tr>
			<td>
			<input type="checkbox" class="bscheck" data="id:${item.id}">
			</td>
			<td>
			    ${item.panelId}
			</td>
			<td>
			    ${item.type}
			</td>
			<td>
			    ${item.productId}
			</td>
			<td>
			    ${item.sortOrder}
			</td>
			<td>
			    ${item.fullUrl}
			</td>
			<td>
			    ${item.picUrl}
			</td>
			<td>
			    ${item.picUrl2}
			</td>
			<td>
			    ${item.picUrl3}
			</td>
			<td>
			    ${item.created?string('yyyy-MM-dd HH:mm:ss')}
			</td>
			<td>
			    ${item.updated?string('yyyy-MM-dd HH:mm:ss')}
			</td>
			<td>
            	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/mall/panel/content/input/${item.id}','修改t_mall_panel_content','${tabId}');">修改</@button>
				<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/mall/panel/content/delete/${item.id}','删除t_mall_panel_content？',false,{id:'${tabId}'});">删除</@button>
            </td>
		</tr>
		</#list>
	</tbody>
</@list>